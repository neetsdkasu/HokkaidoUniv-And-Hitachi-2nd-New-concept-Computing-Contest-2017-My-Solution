// Try Hokkaido Univ.& Hitachi 2nd New-concept Computing Contest 2017
// author: Leonardone @ NEETSDKASU

fn main() {

    use std::time::{Duration, SystemTime};
    let loop_out_time = SystemTime::now() + Duration::from_millis(29000);
    
    let mut stdin = String::new();
    {
        use std::io::Read;
        std::io::stdin().read_to_string(&mut stdin).expect("failed to read string");    
    }
    let mut stdin = stdin.split_whitespace();
    
    macro_rules! get {
        () => ( stdin.next().unwrap().parse().unwrap() );
        ($t:ty) => ( stdin.next().unwrap().parse::<$t>().unwrap() );
    }

    let v = get!(usize);
    let e = get!(u32);
    
    let mut table = vec![vec![0; v+1]; v+1];
    for _ in 0..e {
        let a = get!(usize);
        let b = get!(usize);
        table[a][b] = 100;
        table[b][a] = 100;
    }
    let table = table;
    
    let vemb = get!(usize);
    let s = (vemb as f64).sqrt() as usize;
    
    let (gc, group, conn) = make_group(v, vemb, s);
    
    let mut gvs = vec![0; gc+1];
    let mut gan = vec![0; gc+1];
    for i in 1..v+1 {
        gvs[i] = i;
        gan[i] = i;
    }
    
    macro_rules! calc { ($g:expr) => {{
        let a = gan[$g];
        let mut tmp = 0;
        for &h in &conn[$g] {
            let b = gan[h];
            tmp += table[a][b];
        }
        tmp
    }}}

    let mut xsft: u64 = 88172645463325252;
    
    macro_rules! gen { () => {
            {
                xsft = xsft ^ (xsft << 13);
                xsft = xsft ^ (xsft >> 17);
                xsft = xsft ^ (xsft << 5);
                xsft
            }
        };
    }
    
    let mut score = {
        let mut tmp = 0;
        for g in 1..gc+1 {
            tmp += calc!(g);
        }
        tmp / 2
    };
    let mut max_score = score;
    
    let alpha = 0.8_f64;
    let max_loop = 400_000_000_i32;
    let mut lc = max_loop;
    
    loop {
        for g1 in 1..gc {
            let zr = gan[g1] == 0;
            for g2 in g1+1..gc+1 {
                if zr && gan[g2] == 0 { continue; }
                lc -= 1;
                let old_score = calc!(g1) + calc!(g2);
                gan.swap(g1, g2);
                let new_score = calc!(g1) + calc!(g2);
                let tmp_score = score + new_score - old_score;
                if score > tmp_score {
                    if new_score == 0 {
                        gan.swap(g1, g2);
                        continue;
                    }
                    let te = alpha.powf((max_loop - lc) as f64 / max_loop as f64);
                    let ee = ((tmp_score as f64 - score as f64) / te).exp();
                    let pp = gen!() as f64 / std::u64::MAX as f64;
                    if pp > ee {
                        gan.swap(g1, g2);
                        continue;
                    }
                }
                score = tmp_score;
                if score > max_score {
                    max_score = score;
                    gvs.clone_from(&gan);
                }
            }
        }
        let now = SystemTime::now();
        if now.cmp(&loop_out_time) == std::cmp::Ordering::Greater {
            // println!("loop: {} (max_loop: {}) rate: {}", max_loop - lc, max_loop, (max_loop - lc) as f64 / max_loop as f64);
            break;
        }
    }
    
    
    gan = gvs;
    for _ in 0..20 {
        for g1 in 1..gc {
            let zr = gan[g1] == 0;
            for g2 in g1+1..gc+1 {
                if zr && gan[g2] == 0 { continue; }
                let old_score = calc!(g1) + calc!(g2);
                gan.swap(g1, g2);
                let new_score = calc!(g1) + calc!(g2);
                if old_score > new_score {
                    gan.swap(g1, g2);
                }
            }
        }
    }
    gvs = gan;

    let mut res = vec![0; v];
    for g in 1..gvs.len() {
        let vt = gvs[g];
        if vt == 0 { continue; }
        res[vt-1] = g;
    }
        
    for g in res {
        print!("{}", group[g].len());
        for vt in &group[g] {
            print!(" {}", vt);
        }
        println!();
    }
    
}    
    
fn make_group(v: usize, vemb: usize, s: usize) -> (usize, Vec<Vec<usize>>, Vec<Vec<usize>>) {
    use std::cmp::Ordering;
    
    #[derive(Copy, Clone, Eq, PartialEq)]
    struct Vertex {
        pos: usize,
        x: i32,
        y: i32,
        dist: i32,
    }
    
    impl Ord for Vertex {
        fn cmp(&self, other: &Vertex) -> Ordering { self.dist.cmp(&other.dist) }
    }
    
    impl PartialOrd for Vertex {
        fn partial_cmp(&self, other: &Vertex) -> Option<Ordering> { Some(self.cmp(other)) }
    }
    
    let mut vs = vec![];
    for i in 0..vemb {
        let x = (i % s) as i32;
        let y = (i / s) as i32;
        let dx = x - s as i32 / 2;
        let dy = y - s as i32 / 2;
        vs.push(Vertex{ pos: i, x: x, y: y, dist: dx * dx + dy * dy });
    }
    
    vs.sort();
    
    let mut group = vec![0; vemb];
    let mut gc = 0;
    let mut rem = vemb;
    for vt in vs {
        if group[vt.pos] != 0 { continue; }
        if rem + gc <= v { break; }
        gc += 1;
        group[vt.pos] = gc;
        rem -= 1;
        let x = vt.x + if vt.x % 2 == 0 { 1 } else { -1 };
        let y = vt.y + if vt.y % 2 == 0 { 1 } else { -1 };
        if x < 0 || x >= s as i32 || y < 0 || y >= s as i32 {
            continue;
        }
        if rem + gc <= v { break; }
        let p = y * s as i32 + x;
        group[p as usize] = gc;
        rem -= 1;
    }
    for i in 0..group.len() {
        if group[i] == 0 {
            gc += 1;
            group[i] = gc;
        }
    }
    
    use std::collections::HashSet;
    
    let mut conn: Vec<HashSet<usize>> = vec![HashSet::new(); gc+1];
    
    for i in 0..group.len() {
        let g = group[i];
        if (i+1) < vemb && (i+1) % s != 0 {
            conn[g].insert(group[i+1]); // right
            if (i+s+1) < vemb {
                conn[g].insert(group[i+s+1]); // right-down
            }
            if i >= s-1 {
                conn[g].insert(group[i-(s-1)]); // right-up
            }
        }
        if i % s != 0 {
            conn[g].insert(group[i-1]); // left
            if (i+s-1) < vemb {
                conn[g].insert(group[i+s-1]); // left-down
            }
            if i >= s+1 {
                conn[g].insert(group[i-(s+1)]); // left-up
            }
        }
        if i+s < vemb {
            conn[g].insert(group[i+s]); // down
        }
        if i >= s {
            conn[g].insert(group[i-s]); // up
        }
        conn[g].remove(&g);
    }
    let conn = conn.iter().map(|xs| {
        let mut vs = vec![];
        for &x in xs {
            vs.push(x);
        }
        vs
    }).collect();
    
    let group = {
        let mut tmp = vec![vec![]; gc+1];
        for i in 0..group.len() {
            let g = group[i];
            tmp[g].push(i+1);
        }
        tmp
    };
    
    (gc, group, conn)
}
